import { LoginService } from './services/login.service';
import { MessageService } from './services/message.service';
import { AuthConfig, OAuthService, NullValidationHandler } from 'angular-oauth2-oidc';
import { Component } from '@angular/core';
import * as $ from 'jquery';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]

})
export class AppComponent {
  title = 'keycloak-frontend';
  location: Location;

  username: string;
  isLogged: boolean;
  isAdmin: boolean;

  constructor(
    private oauthService: OAuthService,
    private messageService: MessageService,
    private loginService: LoginService
  ) {
    this.configure();
  }

  ngOnInit() {
                 //Toggle Click Function
     $("#menu-toggle").click(function(e) {
       e.preventDefault();
       $("#wrapper").toggleClass("toggled");
     });
     
   }
   
  authConfig: AuthConfig = {
    issuer: 'http://localhost:8180/auth/realms/tutorial',
    redirectUri: window.location.origin,
    clientId: 'tutorial-frontend',
    responseType: 'code',
    scope: 'openid profile email offline_access',
    showDebugInformation: true,
  };

  configure(): void {
    this.oauthService.configure(this.authConfig);
    this.oauthService.tokenValidationHandler = new NullValidationHandler();
    this.oauthService.setupAutomaticSilentRefresh();
    this.oauthService.loadDiscoveryDocument().then(() => this.oauthService.tryLogin())
      .then(() => {
        if (this.oauthService.getIdentityClaims()) {
          this.isLogged = this.loginService.getIsLogged();
          this.isAdmin = this.loginService.getIsAdmin();
          this.username = this.loginService.getUsername();
          this.messageService.sendMessage(this.loginService.getUsername());
        }
      });
  }
  
  /*nose():void {

    if (this.location.path() == "informes/TEST") {
      this.ocultarMenu();
    } else {
        ///ocultarMenu();
    }
  };
  ocultarMenu(): void {
    $("#wrapper").removeClass("active");//    <div id="wrapper" ng-class="{'active':!esLogin()}">
    $(window).trigger("resize");
  }*/
  ocultarMenu(): void {
    $("#wrapper").toggleClass("toggled");
    $("#wrapper").removeClass("container-fluid");//    <div id="wrapper" ng-class="{'active':!esLogin()}">
    $(window).trigger("resize");
  }

}
